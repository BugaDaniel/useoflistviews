package com.example.dan.uselistview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;



public class DisplayInfoActivity extends AppCompatActivity {

    TextView txtView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_row);
        txtView = (TextView) findViewById(R.id.person_values);
        Intent intent = getIntent();
        String phoneNr = intent.getStringExtra("phoneNR").toString();
        txtView.setText(phoneNr);

    }
}
