package com.example.dan.uselistview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayPhoneActivity extends AppCompatActivity {
    TextView txtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_phone);
        txtView = (TextView) findViewById(R.id.person_values);
        Intent intent = getIntent();
        String phoneNr = intent.getStringExtra("phoneNR").toString();
        txtView.setText(phoneNr);
    }
}
