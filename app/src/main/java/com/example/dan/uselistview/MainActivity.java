package com.example.dan.uselistview;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Person person1 = new Person("Sergiu", "Alexandrescu", "074222222");
    private Person person2 = new Person("Dan","Buga", "0745084687");
    private ArrayList<Person> persons = new ArrayList<>();
    private ListView simpleList;

    public  ArrayList<Person> getPersonList(){
        return persons;
    }

    public ListView getSimpleList() {
        return simpleList;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        persons.add(person1);
        persons.add(person2);
        simpleList = (ListView) findViewById(R.id.list_view);
        final CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), persons);
        final TextView txtView;
        txtView = customAdapter.getTxtV();
        simpleList.setAdapter(customAdapter);
        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Person temp =(Person) parent.getItemAtPosition(position);
                String phone = temp.getPhone();


                Intent intent = new Intent(MainActivity.this, DisplayInfoActivity.class);
                intent.putExtra("phoneNR", phone);
                startActivity(intent);

            }
        });


    }
}
